## Leslie's Pool Supplies - Interview Product Catalog Project

### Requirements:
- PHP 7.1.x (http://www.php.net/)
- Composer (https://getcomposer.org/)
- Vagrant 2.0 (https://www.vagrantup.com/)
- VirtualBox 5.1 (https://www.virtualbox.org/)
- Node.js & npm  (https://nodejs.org/)

###Launching a new environment

    leslie/$ cd Code/
    leslie/Code$ composer install
    leslie/Code$ npm install
    leslie/Code$ cd ..
    leslie/$ composer install
    leslie/$ vagrant up
    leslie/$ vagrant ssh
    ~$ cd leslie/Code
    ~/leslie/Code$ php artisan migrate
    ~/leslie/Code$ php artisan scout:mysql-index App\\Product
    ~/leslie/Code$ php artisan scout:mysql-index App\\ProductType
    ~/leslie/Code$ php artisan scout:mysql-index App\\ProductBrand
    
