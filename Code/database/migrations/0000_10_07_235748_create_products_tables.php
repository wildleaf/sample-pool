<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        /*Schema::dropIfExists('product_brands');
        Schema::dropIfExists('product_types');
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_images');*/

        Schema::create('product_brands', function (Blueprint $table) {
            $table->increments('id');

            $table->string('short_name', 50)->unique();
            $table->string('display_name', 255)->default('');
            $table->longText('description')->nullable();
            $table->integer('order')->nullable();

            $table->timestamps();
        });

        Schema::create('product_types', function (Blueprint $table) {
            $table->increments('id');

            $table->string('short_name', 50)->unique();
            $table->string('display_name', 255)->default('');
            $table->longText('description')->nullable();
            $table->integer('order')->nullable();

            $table->timestamps();
        });

        Schema::create('products', function (Blueprint $table) {
            // we're not gonna auto-increment ID here as API is authoritative source
            $table->integer('id');
            $table->primary('id');

            // foreign keys
            $table->integer('type_id');
            //$table->foreign('type_id')->references('id')->on('product_types');
            $table->integer('brand_id');
            //$table->foreign('brand_id')->references('id')->on('product_brands');

            $table->string('name', 255);
            $table->longText('description');
            $table->boolean('aboveground');

            $table->timestamps();

        });

        Schema::create('product_images', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('product_id');

            $table->string('url', 255);
            $table->string('alt', 255)->default('');
            $table->string('title', 255)->default('');
            $table->integer('order')->nullable();

            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();/*
        Schema::dropIfExists('product_brands');
        Schema::dropIfExists('product_types');
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_images');*/
        Schema::enableForeignKeyConstraints();
    }
}
