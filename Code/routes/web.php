<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', 'Store\CatalogController@listProducts')->name('home');
Route::get('/product/{product_id}', 'Store\CatalogController@displayProduct')->name('product_view');

Route::get('/search', 'Store\CatalogController@searchProducts')->name('search');

Route::get('/ingest', 'Store\CatalogController@ingestProducts')->name('ingest');

Route::get('/brand/{brand_id}', 'Store\CatalogController@brandDetails')->name('brand_view');
Route::get('/type/{type_id}', 'Store\CatalogController@typeDetails')->name('type_view');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
