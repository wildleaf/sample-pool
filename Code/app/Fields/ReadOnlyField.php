<?php

namespace App\Fields;
use Illuminate\Support\Facades\Form;
use Zofe\Rapyd\Rapyd;
use Zofe\Rapyd\DataForm\Field\Field;

class ReadOnlyField extends Field {

    public $type = 'readonlyfield';

    public function build() {
        if (parent::build() === false) return;

        $this->output = "<b>" . $this->value . "<input type='hidden' name='$this->name'  value='$this->value'></b>";
    }
}