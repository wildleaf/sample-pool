<?php

namespace App\Fields\Tracker;
use Illuminate\Support\Facades\Form;
use Zofe\Rapyd\Rapyd;
use Zofe\Rapyd\DataForm\Field\Field;
use \PragmaRX\Tracker\Vendor\Laravel\Models\Agent as Agent;

class TrackerAgentField extends Field {

    public $type = 'trackeragentfield';

    public function build() {
        if (parent::build() === false) return;

        $agent = Agent::find($this->value);

        if(file_exists(public_path("/images/admin/tracker/browsers/$agent->browser.png"))) {
            $browser_image = "/images/admin/tracker/browsers/" . $agent->browser . ".png";
        }
        else {
            $browser_image = "/images/admin/tracker/browsers/Other.png";
        }
        $this->output = "<img src='$browser_image' style='width:32px; height:32px; float: left; margin: 0 8px;'>";
        $this->output .= "<b>" . $agent->browser . " " . $agent->browser_version . "</b>" ;
        $this->output .= "<br /><span style='color:#555;font-size:0.8em'>" . $agent->name . '</span>';
        $this->output .= "<input type='hidden' name='$this->name'  value='$this->value'>";


    }
}