<?php

namespace App\Fields\Tracker;
use Illuminate\Support\Facades\Form;
use Zofe\Rapyd\Rapyd;
use Zofe\Rapyd\DataForm\Field\Field;
use \PragmaRX\Tracker\Vendor\Laravel\Models\Log as TrackerLog;

class TrackerLogField extends Field {

    public $type = 'trackerlogfield';

    public function build() {
        if (parent::build() === false) return;

        $logs = TrackerLog::where('session_id', $this->value)->orderBy('created_at', 'ASC')->get();

        foreach($logs as $log) {
            if(!empty($log->routePath->path)) {
                $this->output .= "<div class='col-xs-12'>";
                $this->output .= $log->created_at . " - ";
                if (!empty($log->routePath->path)) {
                    $this->output .= $log->routePath->path . "<br />";
                }
                if (!empty($log->routePath->route->action)) {
                    $this->output .= "<span style='color:#555;font-size:0.8em'>" . $log->routePath->route->action . "</span>";
                }
                $this->output .= "</div>";
            }
        }

        $this->output .= "<input type='hidden' name='$this->name'  value='$this->value'>";


    }
}