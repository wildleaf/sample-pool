<?php

namespace App\Fields;
use Illuminate\Support\Facades\Form;
use Zofe\Rapyd\Rapyd;
use Zofe\Rapyd\DataForm\Field\Field;
use App\Models\Product;

class ProductImagesField extends Field {

    public $type = 'productimagesfield';

    public function build() {
        if (parent::build() === false) return;

        $images = Product::find($this->value)->images;

        foreach($images as $image) {
            $this->output .= "<div class='col-xs-6 col-md-4'><img src=\"$image->url\" style='max-width:100%'/></div>";
        }

    }
}