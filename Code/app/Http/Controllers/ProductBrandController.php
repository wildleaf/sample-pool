<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;
use App\Fields\ReadOnlyField;

use Illuminate\Http\Request;

class ProductBrandController extends CrudController{

    public function all($entity){
        parent::all($entity); 


        $this->filter = \DataFilter::source(new \App\Models\ProductBrand);
        $this->filter->add('short_name', 'Short Name', 'text');
        $this->filter->submit('search');
        $this->filter->reset('reset');
        $this->filter->build();

        $this->grid = \DataGrid::source($this->filter);
        $this->grid->add('id', 'ID', true);
        $this->grid->add('short_name', 'Short Name', true);
        $this->grid->add('display_name', 'Display Name', true);
        $this->addStylesToGrid();
        $this->grid->edit('edit', trans('panel::fields.edit'), 'show|modify');


        $this->grid->paginate(20);

        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);


			$this->edit = \DataEdit::source(new \App\Models\ProductBrand());

			$this->edit->label('Edit Brand');

			$this->edit->add('short_name', 'Short Name', 'App\Fields\ReadOnlyField');

            $this->edit->add('display_name', 'Display Name', 'text');

            $this->edit->add('description', 'Description', 'redactor');


        return $this->returnEditView();
    }    
}
