<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class ProductController extends CrudController{

    public function all($entity){
        parent::all($entity); 


        $this->filter = \DataFilter::source(new \App\Models\Product);
        $this->filter->add('name', 'Name', 'text');
        $this->filter->submit('search');
        $this->filter->reset('reset');
        $this->filter->build();

        $this->grid = \DataGrid::source($this->filter);
        $this->grid->add('id', 'ID', true);
        $this->grid->add('name', 'Name', true);
        $this->addStylesToGrid();
        $this->grid->edit('edit', trans('panel::fields.edit'), 'show|modify');


        $this->grid->paginate(20);

        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);

        $this->edit = \DataEdit::source(new \App\Models\Product());
        $this->edit->label('Edit Product');
        $this->edit->add('name', 'Name', 'App\Fields\ReadOnlyField');
        $this->edit->add('type_id','Product Type','select')->options(\App\Models\ProductType::pluck("short_name", "id")->all());
        $this->edit->add('brand_id','Brand','select')->options(\App\Models\ProductBrand::pluck("short_name", "id")->all());
        $this->edit->add('id', 'Images', 'App\Fields\ProductImagesField');
        $this->edit->add('description', 'Description', 'redactor');
       
        return $this->returnEditView();
    }    
}
