<?php

namespace App\Http\Controllers\Store;

use App\Models\ProductBrand;
use App\Models\ProductType;
use App\Models\Product;
use App\Models\ProductImage;
use GuzzleHttp\Client as GuzzleClient;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request as Request;

class CatalogController extends Controller
{
    /**
     * list all products
     *
     * @param  Request  $id
     * @return Response
     */
    public function listProducts(Request $request)
    {
        //$request->session()->flash('alert-success', 'Success!');

        $products = Product::orderBy('type_id', 'ASC')->get();

        $result_text = "";
        return view('catalog.list', ['products'=>$products, 'request'=>'']);
    }

    public function searchProducts(Request $request) {
        $search  =  Product::search($request->search_terms);

        if($request->brand_id) {
            $search->where('brand_id', $request->brand_id);
        }

        if($request->type_id) {
            $search->where('type_id', $request->type_id);
        }

        if($request->aboveground) {
            $search->where('aboveground', '1');
        }

        $search->orderBy('type_id');

        return view('catalog.list', ['products'=>$search->get(), 'request'=>$request]);

    }

    public function brandDetails(Request $request, $brand_id) {
        $request->brand_id = $brand_id;
        return $this->searchProducts($request);
    }

    public function typeDetails(Request $request, $type_id) {
        $request->type_id = $type_id;
        return $this->searchProducts($request);
    }

    /**
     * display a product
     *
     * @param  Request  $id
     * @return Response
    */
    public function displayProduct(Request $request, $product_id)
    {
        //$request->session()->flash('alert-success', 'Success!');

        $product = Product::find($product_id);

        return view('catalog.product', ['product'=>$product]);
    }

    /**
     * ingest all products - DONT WORRY THIS WILL BE MOVED TO ADMIN/CRON JOB
     *
     * @param  Request  $request
     * @return Response
     */
    public function ingestProducts(Request $request)
    {
        $guzzle_client = new GuzzleClient(['headers' => ['authkey'=>'M0UHF433MFO11PP-aaron']]);

        $list_response = $guzzle_client->request('GET','http://www.poolsupplyworld.com/api.cfm');

        $result_text = "";
        if ($list_response->getBody()) {
            $result_text = $list_response->getBody();

            if($product_ids = \GuzzleHttp\json_decode($result_text)) {
                foreach($product_ids as $product_id) {
                    $prod_response = $guzzle_client->request('GET','http://www.poolsupplyworld.com/api.cfm?productid='.$product_id);

                    if($prod_response->getBody()) {

                        $ingest_product = \GuzzleHttp\json_decode($prod_response->getBody());


                        $product = Product::firstOrNew(array('id'=>$ingest_product->id));
                        $product->id = $ingest_product->id;

                        if(!empty($ingest_product->type)) {
                            $type = ProductType::firstOrNew(array('short_name' => $ingest_product->type));
                            $type->save();
                            $product->type_id = $type->id;
                        }

                        if(!empty($ingest_product->brand)) {
                            $brand = ProductBrand::firstOrNew(array('short_name' => $ingest_product->brand));
                            $brand->save();
                            $product->brand_id = $brand->id;
                        }

                        $product->aboveground = !empty($ingest_product->aboveground) ? $ingest_product->aboveground : false;
                        $product->description = !empty($ingest_product->description) ? $ingest_product->description : "";
                        $product->name = !empty($ingest_product->name) ? $ingest_product->name : "Undefined";
                        $product->save();

                        if(!empty($ingest_product->images)) {
                            foreach($ingest_product->images as $img_url) {
                                $image = ProductImage::firstOrNew(array('url' => $img_url));
                                $image->url = $img_url;
                                $image->product_id = $product->id;
                                $image->save();
                            }
                        }

                        $request->session()->flash('alert-success', 'Saved product #' . $product_id);

                    }
                    else {
                        $request->session()->flash('alert-error', 'Could not retrieve product #' . $product_id);

                    }

                }
            }
        }
        else {
            $request->session()->flash('alert-error', 'Could not retrieve product listings');
        }


        return view('catalog.ingest', ['products'=>$result_text]);
    }
}