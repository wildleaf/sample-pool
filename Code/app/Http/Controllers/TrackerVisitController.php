<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;
use App\Fields\ReadOnlyField;

use Illuminate\Http\Request;

class TrackerVisitController extends CrudController{

    public function all($entity){
        parent::all($entity);


        $this->filter = \DataFilter::source(new \PragmaRX\Tracker\Vendor\Laravel\Models\Session);
        $this->filter->add('client_ip', 'IP Address', 'text');
        $this->filter->submit('search');
        $this->filter->reset('reset');
        $this->filter->build();

        $this->grid = \DataGrid::source($this->filter);
        $this->grid->add('id', 'ID', true);
        $this->grid->add('client_ip', 'IP Address', true);
        $this->grid->add('created_at', 'Session Start', true);
        $this->addStylesToGrid();
        $this->grid->edit('edit', 'View', 'show');


        $this->grid->paginate(20);

        return $this->returnView();
    }

    public function  edit($entity){

        parent::edit($entity);


        $this->edit = \DataEdit::source(new \PragmaRX\Tracker\Vendor\Laravel\Models\Session());

        $this->edit->label('Tracker Visit');

        $this->edit->add('client_ip', 'IP Address', 'App\Fields\ReadOnlyField');
        $this->edit->add('uuid', 'UUID', 'App\Fields\ReadOnlyField');
        $this->edit->add('agent_id', 'Agent', 'App\Fields\Tracker\TrackerAgentField');
        $this->edit->add('id', 'Log', 'App\Fields\Tracker\TrackerLogField');


        return $this->returnEditView();
    }
}
