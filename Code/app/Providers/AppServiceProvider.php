<?php

namespace App\Providers;

use App\Models\ProductType;
use App\Models\ProductBrand;
use View;
use Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        if(Schema::hasTable('product_types')) {
            View::share('store_product_types', ProductType::getFriendlyArray());
        }
        else {
            View::share('store_product_types', array());
        }
        if(Schema::hasTable('product_brands')) {
            View::share('store_product_brands', ProductBrand::getFriendlyArray());
        }
        else {
            View::share('store_product_brands', array());
        }

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
