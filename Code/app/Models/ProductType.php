<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    use Searchable;
    //
    protected $fillable = ['short_name', 'display_name'];
    protected $appends = ['displayname'];

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function getDisplaynameAttribute() {
        return !empty($this->display_name) ? $this->display_name : ucwords(str_plural($this->short_name));
    }


    static public function getFriendlyArray() {
        $types = ProductType::orderBy('short_name', 'ASC')->get();

        $type_list = [];
        foreach($types as $type) {
            $type_list[$type->id] = $type;
        }
        return $type_list;
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }

}
