<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    //
    protected $fillable = ['short_name', 'display_name'];


    function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

}
