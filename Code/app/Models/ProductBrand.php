<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class ProductBrand extends Model
{
    //
    use Searchable;

    protected $fillable = ['short_name', 'display_name'];
    protected $appends = ['displayname'];

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function getDisplaynameAttribute() {
        return !empty($this->display_name) ? $this->display_name : ucwords($this->short_name);
    }

    static public function getFriendlyArray() {
        $brands = ProductBrand::orderBy('short_name', 'ASC')->get();;

        $brand_list = [];
        foreach($brands as $brand) {
            $brand_list[$brand->id] = $brand;
        }


        return $brand_list;
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }

}
