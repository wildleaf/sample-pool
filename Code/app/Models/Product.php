<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Searchable;
    //
    public $timestamps = true;
    protected $fillable = ['name', 'description', 'aboveground', 'type_id', 'brand_id'];

    function type()
    {
        return $this->belongsTo('App\Models\ProductType');
    }

    function brand()
    {
        return $this->belongsTo('App\Models\ProductBrand');
    }

    public function images()
    {
        return $this->hasMany('App\Models\ProductImage');
    }


    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }
}
