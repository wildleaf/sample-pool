<?php
namespace App\Observers;

use App\Models\TrackerVisit;

class TrackerVisitObserver
{

    /**
     * Listen to the TrackerVisit creating event.
     *
     * @param  TrackerVisit  $TrackerVisit
     * @return void
     */
    public function creating(TrackerVisit $TrackerVisit)
    {
        //code...
    }

    /**
     * Listen to the TrackerVisit created event.
     *
     * @param  TrackerVisit  $TrackerVisit
     * @return void
     */
    public function created(TrackerVisit $TrackerVisit)
    {
        //code...
    }

    /**
     * Listen to the TrackerVisit updating event.
     *
     * @param  TrackerVisit  $TrackerVisit
     * @return void
     */
    public function updating(TrackerVisit $TrackerVisit)
    {
        //code...
    }

    /**
     * Listen to the TrackerVisit updated event.
     *
     * @param  TrackerVisit  $TrackerVisit
     * @return void
     */
    public function updated(TrackerVisit $TrackerVisit)
    {
        //code...
    }

    /**
     * Listen to the TrackerVisit saving event.
     *
     * @param  TrackerVisit  $TrackerVisit
     * @return void
     */
    public function saving(TrackerVisit $TrackerVisit)
    {
        //code...
    }

    /**
     * Listen to the TrackerVisit saved event.
     *
     * @param  TrackerVisit  $TrackerVisit
     * @return void
     */
    public function saved(TrackerVisit $TrackerVisit)
    {
        //code...
    }

    /**
     * Listen to the TrackerVisit deleting event.
     *
     * @param  TrackerVisit  $TrackerVisit
     * @return void
     */
    public function deleting(TrackerVisit $TrackerVisit)
    {
        //code...
    }

    /**
     * Listen to the TrackerVisit deleted event.
     *
     * @param  TrackerVisit  $TrackerVisit
     * @return void
     */
    public function deleted(TrackerVisit $TrackerVisit)
    {
        //code...
    }

    /**
     * Listen to the TrackerVisit restoring event.
     *
     * @param  TrackerVisit  $TrackerVisit
     * @return void
     */
    public function restoring(TrackerVisit $TrackerVisit)
    {
        //code...
    }

    /**
     * Listen to the TrackerVisit restored event.
     *
     * @param  TrackerVisit  $TrackerVisit
     * @return void
     */
    public function restored(TrackerVisit $TrackerVisit)
    {
        //code...
    }
}