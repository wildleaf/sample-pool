<?php
namespace App\Observers;

use App\Models\Product;

class ProductObserver
{
    
    /**
     * Listen to the Product creating event.
     *
     * @param  Product  $Product
     * @return void
     */
    public function creating(Product $Product)
    {
        //code...
    }

     /**
     * Listen to the Product created event.
     *
     * @param  Product  $Product
     * @return void
     */
    public function created(Product $Product)
    {
        //code...
    }

    /**
     * Listen to the Product updating event.
     *
     * @param  Product  $Product
     * @return void
     */
    public function updating(Product $Product)
    {
        //code...
    }

    /**
     * Listen to the Product updated event.
     *
     * @param  Product  $Product
     * @return void
     */
    public function updated(Product $Product)
    {
        //code...
    }

    /**
     * Listen to the Product saving event.
     *
     * @param  Product  $Product
     * @return void
     */
    public function saving(Product $Product)
    {
        //code...
    }

    /**
     * Listen to the Product saved event.
     *
     * @param  Product  $Product
     * @return void
     */
    public function saved(Product $Product)
    {
        //code...
    }

    /**
     * Listen to the Product deleting event.
     *
     * @param  Product  $Product
     * @return void
     */
    public function deleting(Product $Product)
    {
        //code...
    }

    /**
     * Listen to the Product deleted event.
     *
     * @param  Product  $Product
     * @return void
     */
    public function deleted(Product $Product)
    {
        //code...
    }

    /**
     * Listen to the Product restoring event.
     *
     * @param  Product  $Product
     * @return void
     */
    public function restoring(Product $Product)
    {
        //code...
    }

    /**
     * Listen to the Product restored event.
     *
     * @param  Product  $Product
     * @return void
     */
    public function restored(Product $Product)
    {
        //code...
    }
}