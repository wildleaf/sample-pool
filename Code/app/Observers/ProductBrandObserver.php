<?php
namespace App\Observers;

use App\ProductBrand;

class ProductBrandObserver
{
    
    /**
     * Listen to the ProductBrand creating event.
     *
     * @param  ProductBrand  $ProductBrand
     * @return void
     */
    public function creating(ProductBrand $ProductBrand)
    {
        //code...
    }

     /**
     * Listen to the ProductBrand created event.
     *
     * @param  ProductBrand  $ProductBrand
     * @return void
     */
    public function created(ProductBrand $ProductBrand)
    {
        //code...
    }

    /**
     * Listen to the ProductBrand updating event.
     *
     * @param  ProductBrand  $ProductBrand
     * @return void
     */
    public function updating(ProductBrand $ProductBrand)
    {
        //code...
    }

    /**
     * Listen to the ProductBrand updated event.
     *
     * @param  ProductBrand  $ProductBrand
     * @return void
     */
    public function updated(ProductBrand $ProductBrand)
    {
        //code...
    }

    /**
     * Listen to the ProductBrand saving event.
     *
     * @param  ProductBrand  $ProductBrand
     * @return void
     */
    public function saving(ProductBrand $ProductBrand)
    {
        //code...
    }

    /**
     * Listen to the ProductBrand saved event.
     *
     * @param  ProductBrand  $ProductBrand
     * @return void
     */
    public function saved(ProductBrand $ProductBrand)
    {
        //code...
    }

    /**
     * Listen to the ProductBrand deleting event.
     *
     * @param  ProductBrand  $ProductBrand
     * @return void
     */
    public function deleting(ProductBrand $ProductBrand)
    {
        //code...
    }

    /**
     * Listen to the ProductBrand deleted event.
     *
     * @param  ProductBrand  $ProductBrand
     * @return void
     */
    public function deleted(ProductBrand $ProductBrand)
    {
        //code...
    }

    /**
     * Listen to the ProductBrand restoring event.
     *
     * @param  ProductBrand  $ProductBrand
     * @return void
     */
    public function restoring(ProductBrand $ProductBrand)
    {
        //code...
    }

    /**
     * Listen to the ProductBrand restored event.
     *
     * @param  ProductBrand  $ProductBrand
     * @return void
     */
    public function restored(ProductBrand $ProductBrand)
    {
        //code...
    }
}