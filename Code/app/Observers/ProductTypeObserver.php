<?php
namespace App\Observers;

use App\ProductType;

class ProductTypeObserver
{
    
    /**
     * Listen to the ProductType creating event.
     *
     * @param  ProductType  $ProductType
     * @return void
     */
    public function creating(ProductType $ProductType)
    {
        //code...
    }

     /**
     * Listen to the ProductType created event.
     *
     * @param  ProductType  $ProductType
     * @return void
     */
    public function created(ProductType $ProductType)
    {
        //code...
    }

    /**
     * Listen to the ProductType updating event.
     *
     * @param  ProductType  $ProductType
     * @return void
     */
    public function updating(ProductType $ProductType)
    {
        //code...
    }

    /**
     * Listen to the ProductType updated event.
     *
     * @param  ProductType  $ProductType
     * @return void
     */
    public function updated(ProductType $ProductType)
    {
        //code...
    }

    /**
     * Listen to the ProductType saving event.
     *
     * @param  ProductType  $ProductType
     * @return void
     */
    public function saving(ProductType $ProductType)
    {
        //code...
    }

    /**
     * Listen to the ProductType saved event.
     *
     * @param  ProductType  $ProductType
     * @return void
     */
    public function saved(ProductType $ProductType)
    {
        //code...
    }

    /**
     * Listen to the ProductType deleting event.
     *
     * @param  ProductType  $ProductType
     * @return void
     */
    public function deleting(ProductType $ProductType)
    {
        //code...
    }

    /**
     * Listen to the ProductType deleted event.
     *
     * @param  ProductType  $ProductType
     * @return void
     */
    public function deleted(ProductType $ProductType)
    {
        //code...
    }

    /**
     * Listen to the ProductType restoring event.
     *
     * @param  ProductType  $ProductType
     * @return void
     */
    public function restoring(ProductType $ProductType)
    {
        //code...
    }

    /**
     * Listen to the ProductType restored event.
     *
     * @param  ProductType  $ProductType
     * @return void
     */
    public function restored(ProductType $ProductType)
    {
        //code...
    }
}