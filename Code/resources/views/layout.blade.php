<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Aaron's Pool Supplies - @yield('title')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC|Mukta" rel="stylesheet">

    <link href="/css/app.css" rel="stylesheet" type="text/css">

    <script src="/js/app.js" type="application/javascript"></script>

    <script src="/js/custom.js" type="application/javascript"></script>
</head>
<body>
<div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse navbar-menubuilder">
            <a class="navbar-brand" href="/">Aaron's Pool Supplies</a>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/">Home</a></li>
                <li class="dropdown"><a href="/products" class="dropdown-toggle" data-toggle="dropdown">Products <b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        @foreach($store_product_types as $product_type)
                            <li><a href="/type/{{ $product_type->id }}">{{ $product_type->displayname }}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li class="dropdown"><a href="/brands" class="dropdown-toggle" data-toggle="dropdown">Brands <b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        @foreach($store_product_brands as $product_brand)
                            <li><a href="/brand/{{ $product_brand->id }}">{{ $product_brand->displayname }}</a></li>
                        @endforeach
                    </ul>
                </li>

                <li class="dropdown navbar-right"><a href="/search" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-search"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <form class="navbar-form" style="padding: 0 20px 20px;" action="/search" method="GET">
                                <label for="search_terms">Search</label>
                                <input type="text" name="search_terms" id="search_terms" class="form-control">

                                <label for="search_type_id">Product Type</label>
                                <select name="type_id" id="search_type_id" class="form-control" style="width: 100%">
                                    <option value="">Any</option>
                                    @foreach($store_product_types as $product_type)
                                        <option value="{{ $product_type->id }}">{{ $product_type->displayname }}</option>
                                    @endforeach
                                </select>

                                <label for="search_brand_id">Brand</label>
                                <select name="brand_id" id="search_brand_id" class="form-control" style="width: 100%">
                                    <option value="">Any</option>
                                    @foreach($store_product_brands as $product_brand)
                                        <option value="{{ $product_brand->id }}">{{ $product_brand->displayname }}</option>
                                    @endforeach
                                </select>
                                <div class="checkbox-inline">
                                <label for="product_aboveground">
                                    <input type="checkbox" class="form-control" name="aboveground" id="product_aboveground" /> Above Ground Only
                                </label>
                                </div>

                                <br />
                                <button class="btn btn-primary btn-lg btn-block" name="topsearch" value="search">Search</button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="container">

    <div class="row">
        <div class="col-xs-12">
        @if (array_key_exists('breadcrumb', View::getSections()))
                <div class="breadcrumb">
                    @yield('breadcrumb')
                </div>
        @endif

        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
            @endforeach
        </div> <!-- end .flash-message -->

        @yield('content')

        </div>


    </div>
</div>

</body>
</html>
