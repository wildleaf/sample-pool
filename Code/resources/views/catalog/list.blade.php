@extends('layout')

@section('title', 'Product List')

@section('breadcrumb')
    @php ($route_name = Route::currentRouteName())

    @if($route_name == "brand_view")
        <a href="/">Home</a> &gt; <a href="/brands/">Brands</a> &gt; {{ $store_product_brands[$request->brand_id]->displayname }}</h1>
    @elseif($route_name == "type_view")
        <a href="/">Home</a> &gt; {{ $store_product_types[$request->type_id]->displayname }}</h1>
    @elseif($route_name == "search")
        <a href="/">Home</a> &gt; Search</h1>
    @else
        Home
    @endif
@endsection

@section('content')

    <div id="product-list-page" class="col-xs-12">
        @php ($curshortname = "")

        @php ($route_name = Route::currentRouteName())

        @if($route_name == "brand_view")
            <h1>{{ $store_product_brands[$request->brand_id]->displayname }}</h1>
            {!! $store_product_brands[$request->brand_id]->description !!}
        @endif

        @if($route_name == "type_view")
            <h1>{{ $store_product_types[$request->type_id]->displayname }}</h1>
            {!! $store_product_types[$request->type_id]->description !!}
        @endif

        @if($route_name == "search")
            <h1>Product Search</h1>
        @endif

        @if(!empty($request->topsearch))
            @if(count($products) > 0)
                <h2>{{count($products)}} results</h2>
                <div class="alert alert-success">
            @else
                <h2>0 results</h2>
                <div class="alert alert-warning">
            @endif
            @if(!empty($request->search_terms))
                    <li>Searching Terms: {{ $request->search_terms }}</li>
            @endif
            @if(!empty($request->brand_id))
                    <li>Brand: {{ $store_product_brands[$request->brand_id]->displayname }}</li>
            @endif
            @if(!empty($request->type_id))
                    <li>Product Type: {{ $store_product_types[$request->type_id]->displayname }} </li>
            @endif
            @if(!empty($request->aboveground))
                    <li>Above Ground Only</li>
            @endif
            </div>
        @endif

        @foreach ($products as $product)
            @if($route_name != "type_view")
            @if($product->type->short_name != $curshortname)
                @if($loop->iteration > 1)
                    </div>
                @endif
                @php ($curshortname = $product->type->short_name)
                <h2>{{ $product->type->displayname }}</h2>
                <div class="row row-eq-height">
            @endif
            @endif
                    <a href="/product/{{ $product->id }}">
                    <div class="col-xs-4 product">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <img src="{{ $product->images[0]->url }}" />
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <h3>{{ $product->name }}</h3>
                                <p>
                                    {{ str_limit($product->description, 80, ' ...') }}
                                </p>
                            </div>
                        </div>
                    </div>
                    </a>

        @endforeach
                </div>

    </div>
@endsection