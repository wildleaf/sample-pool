@extends('layout')

@section('title', 'Product List')

@section('breadcrumb')
    <a href="/">Home</a> &gt; <a href="/type/{{ $product->type->id }}">{{ $product->type->displayname }}</a> &gt; {{ $product->name }}
@endsection

@section('content')

    <div class="container" id="product-page">
        <div class="card">
            <div class="wrapper row">
                <div class="details col-md-7">
                    <h2 class="product-title">{{ $product->name }}</h2>
                    <h3 class="product-brand">@lang('leslie.brand'): <a href="/brand/{{ $product->brand->id }}">{{ $product->brand->short_name }}</a></h3>

                    @if($product->aboveground)
                        <h5>For Above Ground Pools</h5>
                    @endif
                    <div class="product-description">
                        {{ $product->description }}
                    </div>
                </div>

                <div class="preview col-md-5">
                    <div class="preview-pic tab-content">
                        @foreach($product->images as $image)
                            @if($loop->first)
                                <div class="tab-pane active"
                            @else
                                <div class="tab-pane"
                            @endif
                            id="pic-{{$loop->iteration}}"><img src="{{ $image->url }}" /></div>
                        @endforeach
                    </div>
                    <div class="navbar">
                    <ul class="preview-thumbnail nav nav-tabs">
                        @foreach($product->images as $image)
                            @if($loop->first)
                                <li class="active">
                            @else
                                <li>
                            @endif
                            <a data-target="#pic-{{ $loop->iteration }}" data-toggle="tab"><img src="{{ $image->url }}" /></a></li>
                        @endforeach
                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection